<?php
$s = urldecode($URL[1]);
$h1 = 'Tags - '.$s;
$title = 'Tags - '.$s;
$desc = 'Tags - '.$s;
$key = '';
$var = 'Tags';
include('inc/head.php');
?>

<style>
  <? include('css/blog-grid.css'); ?>
</style>
</head>
<body class="blog-grid">
  <?php include('inc/topo-blog.php'); ?>
  <main>
    <div id="pesquisa">
      <section>
        <?= $caminho ?>
        <div class="container">
          <div class="wrapper">
            <?php

            $Read->ExeRead(TB_USERS, "WHERE user_status = :st", "st=0");
            $authors = $Read->getResult();
            
            $blogMonthList = array("Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");

            $Read->ExeRead(TB_BLOG, "WHERE blog_status = :stats AND (blog_keywords LIKE '%' :s '%') ORDER BY blog_id DESC", "s={$s}&stats=2");

            if (!$Read->getResult()):  
            $Read->ExeRead(TB_CATEGORIA, "WHERE cat_status = :stats AND (cat_title LIKE '%' :s '%' OR cat_description LIKE '%' :s '%' OR cat_content LIKE '%' :s '%')", "stats=2&s={$s}");
              if (!$Read->getResult()):
                WSErro("Nenhuma postagem encontrada com a tag <b>{$s}</b>.", WS_INFOR, null, "Resultado da pesquisa");
              else: ?>
                <div class="container pt-0">
                  <h2 class="text-center">Postagens com a Tag: <span class="dark"><?= $s; ?></span></h2>
                  <div class="row justify-content-center gap-30 pt-5">
                    <?php
                    foreach ($Read->getResult() as $cat):
                      extract($cat); ?>
                      <div class="blog-card blog-card--category col-4">
                        <div class="blog-card__image">
                          <a rel="nofollow" href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $cat_name; ?>" title="<?= $cat_title; ?>">
                            <?php
                            if (empty($cat_cover)): ?>
                                <img class="blog-card__cover" src="<?=RAIZ?>/doutor/images/default.png" alt="<?=$cat_title?>" title="<?=$cat_title?>">
                            <? else: ?>
                              <img class="blog-card__cover" src="<?=RAIZ?>/doutor/uploads/<?=$cat_cover?>" alt="<?=$cat_title?>" title="<?=$cat_title?>">
                            <? endif; ?>
                          </a>
                        </div>
                        <div class="blog-card__info">
                          <h3 class="blog-card__title"><a rel="nofollow" href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $cat_name; ?>" title="<?= $cat_title; ?>"><?=$cat_title?></a></h3>
                          <div class="blog-card__description">
                            <p><?= $cat_description; ?></p>
                          </div>
                        </div>
                      </div>
                    <?php endforeach; ?>
                  </div>
                </div>
              <? endif;
            else: ?>
              <div class="container pt-0">
                <h2 class="text-center">Postagens com a Tag: <span class="dark"><?= $s; ?></span></h2>
                <div class="row justify-content-center gap-30 pt-5">
                  <?php
                  foreach ($Read->getResult() as $blog):
                    extract($blog); ?>
                    <div class="blog-card col-4">
                          <div class="blog-card__image">
                            <a rel="nofollow" href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $blog_name; ?>" title="<?= $blog_title; ?>">
                              <img class="blog-card__cover" src="<?=RAIZ?>/doutor/uploads/<?=$blog_cover?>" alt="<?=$blog_title?>" title="<?=$blog_title?>">
                            </a>

                            <?php
                              $blogCatUrl = Check::CatByParent($cat_parent, EMPRESA_CLIENTE);
                              $blogCatUrlFinal = explode("/", $blogCatUrl); 
                              array_pop($blogCatUrlFinal);                       
                              $blogCatTitle = Check::CatByUrl(end($blogCatUrlFinal), EMPRESA_CLIENTE);
                              $blogCatUrl = RAIZ . "/". substr($blogCatUrl, 0, -1);
                            ?>
                            <a class="blog-card__category" href="<?=$blogCatUrl?>" title="<?=$blogCatTitle?>"><?=$blogCatTitle?></a>
                          </div>
                          <div class="blog-card__info">
                            <h3 class="blog-card__title"><a rel="nofollow" href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $blog_name; ?>" title="<?= $blog_title; ?>">
                              <?= $blog_title ?>
                            </a></h3>
                            <?php $newDate = explode("/", date("d/m/Y", strtotime($blog_date)));
                              $blogDay = $newDate[0];
                              $blogMonth = $newDate[1];
                              $blogYear = $newDate[2];
                              $blogFullDate = $blogDay . " de " . $blogMonthList[$blogMonth - 1] . " de " . $blogYear;
                            ?>
                            <p class="blog-card__date"><?= $blogFullDate?></p>
                            <div class="blog-card__author">
                              <?php 
                                $authorKey = array_search($user_id, array_column($authors, 'user_id'));
                                $authorName = $authors[$authorKey]['user_name'];
                              ?>
                              Por:
                              <a href="<?=$url?>autor/<?= urlencode($authorName) ?>" rel="nofollow" title="<?= $authorName ?>"><?= $authorName ?></a>
                            </div>

                            <div class="blog-card__description">
                              <?php if (BLOG_BREVEDESC && isset($blog_brevedescription)) : ?>
                                <p class="blog-card__content-text"><?= $blog_brevedescription ?></p>
                              <?php else : ?>
                                <p class="blog-card__content-text"><?= Check::Words($blog_content, 25); ?></p>
                              <?php endif; ?>
                            </div>
                          </div>
                        </div>
                  <? endforeach; ?>
                </div>
              </div>
            <? endif; ?>
          </div> <!-- wrapper -->
          <div class="clear"></div>
        </div> <!-- container -->
      </section>
    </div> <!-- pesquisa -->
  </main>
  <?php include('inc/footer-blog.php'); ?>
</body>
</html>