        <div class="cd-hero">
    <ul class="cd-hero-slider autoplay">
        <li class="selected">
            <div class="cd-full-width">
                <h2>AÇO 4140</h2>
                <p>Se procura ofertas de Aço 4140, você só encontra na maior vitrine Soluções Industriais, cote produtos online com mais de 30 indústrias gratuitamente para todo o Brasil.</p>
                <a href="<?=$url?>aco-4140" class="cd-btn">Saiba mais</a>
            </div>

        </li>
        <li>
            <div class="cd-full-width">
                <h2>Corte e dobra de chapas</h2>
                <p>Os serviços de corte e dobra de chapas são realizados com base na disponibilidade de guilhotinas e dobradeiras com capacidade de até 1/4.</p>
                <a href="<?=$url?>corte-e-dobra-de-chapas" class="cd-btn">Saiba mais</a>
            </div>

        </li>
        <li>
            <div class="cd-full-width">
                <h2>Equipamentos industriais</h2>
                <p>São muitos os acessórios e equipamentos industriais que podem ser produzidos e adquiridos nos tempos atuais.</p>
                <a href="<?=$url?>equipamentos-industriais" class="cd-btn">Saiba mais</a>
            </div>

        </li>
    </ul>
    <div class="cd-slider-nav">
        <nav>
            <span class="cd-marker item-1"></span>
            <ul>
                <li class="selected"><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
                <li><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
                <li><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
            </ul>
        </nav>
    </div>

</div>



        <!-- Hero End -->