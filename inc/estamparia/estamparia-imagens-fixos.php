
<div class="col-12 pt-4">
<div class="jumbotron py-1">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 py-lg-4 py-3 text-center"><a href="<?= $url ?>imagens/estamparia/estamparia-01.webp" class="lightbox" title="<?= $h1 ?>"><img src="<?= $url ?>imagens/estamparia/thumbs/estamparia-1.webp" class="img-thumbnail mb-1 lazy" alt="<?= $h1 ?>" title="<?= $h1 ?>" /></a><strong>Imagem ilustrativa de <?= $h1 ?></strong></div>
        <div class="col-lg-6 col-md-6 col-sm-12 py-lg-4 py-3 text-center"><a href="<?= $url ?>imagens/estamparia/estamparia-02.webp" title="<?= $h1 ?>" target="_blank"><img src="<?= $url ?>imagens/estamparia/thumbs/estamparia-2.webp" class="img-thumbnail mb-1 lazy" alt="<?= $h1 ?>" title="<?= $h1 ?>" /></a><strong>Imagem ilustrativa de <?= $h1 ?></strong></div>
    </div>
</div>
</div>
    
