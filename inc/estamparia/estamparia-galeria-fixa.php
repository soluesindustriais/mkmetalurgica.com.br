
   
<div class="container p-0 my-3">
<div class="row no-gutters justify-content-center py-3">
    <div class="col-lg-12 pt-2 mt-2 text-center">
        <div id="paginas-destaque" class="owl-carousel owl-theme owl-loaded owl-drag">
            <div class="col-12 py-2">
                <div class="owl-stage-outer">
                    <div class="owl-stage"> <?php $lista = array('Estampagem De Chapas','Estamparia De Chapas De Aço','Estamparia De Chapas Metálicas','Estamparia Ferramentaria','Serviço Estamparia E Ferramentaria');
                                            shuffle($lista);
                                            for ($i = 1; $i < 13; $i++) { ?> <div class="owl-item">
                                <div class="card blog rounded border-0 shadow overflow-hidden">
                                    <div class="position-relative border-intro"><a class="lightbox" href="<?= $url; ?>imagens/estamparia/estamparia-<?= $i ?>.webp" title="<?= $lista[$i] ?>"><img src="<?= $url; ?>imagens/estamparia/thumbs/estamparia-<?= $i ?>.webp" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
                                            <div class="overlay rounded-top bg-dark"></div>
                                            <div class="author"><small class="text-light user d-block"></small><small class="text-light date"><?= $lista[$i] ?></small></div>
                                    </div>
                                </div></a>
                            </div><?php } ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

    