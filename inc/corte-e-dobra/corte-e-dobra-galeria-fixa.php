
   
<div class="container p-0 my-3">
<div class="row no-gutters justify-content-center py-3">
    <div class="col-lg-12 pt-2 mt-2 text-center">
        <div id="paginas-destaque" class="owl-carousel owl-theme owl-loaded owl-drag">
            <div class="col-12 py-2">
                <div class="owl-stage-outer">
                    <div class="owl-stage"> <?php $lista = array('Corte Chapa De Aluminio','Corte Chapa Inox','Corte De Chapa','Corte De Chapa De Aço','Corte De Chapa Galvanizada','Corte De Chapas Metálicas','Dobra Chapa De Aço','Dobra Cnc','Dobra Cnc Em Chapa De Alumínio Preço','Dobra Cnc Em Chapa De Alumínio Valor','Dobra Cnc Em Chapa De Carbono Preço','Dobra Cnc Em Chapa De Carbono Valor','Dobra Cnc Em Chapa De Inox Preço','Dobra De Chapa','Dobra De Chapa De Aluminio','Dobra De Chapa Galvanizada','Dobra De Chapa Inox','Dobra De Chapas De Metal','Dobra De Chapas Metálicas','Empresa De Corte De Chapas','Empresa De Dobra De Chapas','Indústria De Corte De Chapas','Prestação De Serviço Dobra De Chapa','Quanto Custa Dobra Cnc Em Chapa De Alumínio','Quanto Custa Dobra Cnc Em Chapa De Inox','Quanto Custa Dobra Cnc Em Chapa Galvanizada','Serviço De Corte De Chapas','Serviço De Dobra Cnc','Serviço De Dobra Cnc Em Chapa','Serviço De Dobra Cnc Em Chapa De Aluminio','Serviço De Dobra Cnc Em Chapa De Carbono','Serviço De Dobra Cnc Em Chapa De Inox','Serviço De Dobra Cnc Em Chapa Galvanizada','Serviço De Dobra Cnc Em Chapa Valor','Serviço De Dobra De Chapas');
                                            shuffle($lista);
                                            for ($i = 1; $i < 13; $i++) { ?> <div class="owl-item">
                                <div class="card blog rounded border-0 shadow overflow-hidden">
                                    <div class="position-relative border-intro"><a class="lightbox" href="<?= $url; ?>imagens/corte-e-dobra/corte-e-dobra-<?= $i ?>.jpg" title="<?= $lista[$i] ?>"><img src="<?= $url; ?>imagens/corte-e-dobra/thumbs/corte-e-dobra-<?= $i ?>.jpg" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
                                            <div class="overlay rounded-top bg-dark"></div>
                                            <div class="author"><small class="text-light user d-block"></small><small class="text-light date"><?= $lista[$i] ?></small></div>
                                    </div>
                                </div></a>
                            </div><?php } ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

    