
<section class="wrapper card-informativo">
    <span><p>O que nós fornecemos</p></span>
    <h2>Nossos principais focos</h2>
        <div class="cards-informativos">
        <div class="cards-informativos-infos">
            <i class="fa-solid fa-location-dot"></i></i>
            <h3>Missão</h3>
            <p>Dispor aos nossos clientes serviços diferenciados pela alta performance, executado por profissionais que compartilham nossa Visão e Valores.</p>
        </div>
        <div class="cards-informativos-infos">
            <i class="fa-solid fa-eye"></i>
            <h3>Visão</h3>
        <p>Ser referência em soluções de montagens e instalações assépticas, baseados na segurança, qualidade, inovação e comprometimento com nosso maior patrimônio, nosso cliente.</p>
    </div>
    <div class="cards-informativos-infos">
        <i class="fa-solid fa-star"></i>
        <h3>Valores</h3>
        <p>Respeito, Ética, Honestidade, Responsabilidade, Humildade, Cortesia, Flexibilidade, Tolerância e Otimismo.</p>
    </div>
</div>
</section>
