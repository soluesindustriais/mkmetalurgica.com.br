<?php
if (!function_exists('generateSubmenu')) {
  function generateSubmenu($submenuItems, $url) {
    foreach ($submenuItems as $itemName => $itemData) : ?>
      <li <?= isset($itemData['submenu']) ? "class='dropdown'" : "" ?>>
        <a href="<?= strpos($itemData['url'], 'http') !== false ? $itemData['url'] : $url . $itemData['url'] ?>" title="<?= $itemName ?>"<?= isset($itemData['target']) ? 'target="' . $itemData['target'] . '"' : '' ?><?= isset($itemData['rel']) ? 'rel="' . $itemData['rel'] . '"' : '' ?> >
          <?= isset($itemData['icon']) ? "<i class='" . $itemData['icon'] . "'></i><span class='d-inline-block ml-1'>" . $itemName . "</span>" : $itemName ?>
        </a>
      </li>
    <?php endforeach;
  }
}

foreach ($menuItems as $itemName => $itemData) :
  if ($itemName !== "SIG") { ?>
    <li <?= isset($itemData['submenu']) ? "class='dropdown'" : "" ?> <?= isset($itemData['icon']) ? " data-icon-menu" : "" ?>>
      <a href="<?= strpos($itemData['url'], 'http') !== false ? $itemData['url'] : $link_minisite_subdominio . $itemData['url'] ?>" title="<?= $itemName ?>"<?= isset($itemData['target']) ? 'target="' . $itemData['target'] . '"' : '' ?><?= isset($itemData['rel']) ? 'rel="' . $itemData['rel'] . '"' : '' ?> >
        <?= isset($itemData['icon']) ? "<i class='" . $itemData['icon'] . " '></i> $itemName" : $itemName ?> 
        <?= isset($itemData['icon-text']) && isset($itemData['icon']) ? "<span class='d-block mt-2'>" . $itemName . "</span>" : "" ?>
      </a>
        <ul class="<?= $itemName === 'Informações' ? 'sub-menu-info' : 'sub-menu' ?>">
          <?php 
        
            generateSubmenu($itemData['submenu'], $link_minisite);
          ?> 
        </ul>
    </li>
<?php }
endforeach; ?>
