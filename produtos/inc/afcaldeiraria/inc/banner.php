<!-- #Alterar banner -->
<!-- Aqui você deve alterar os títulos e mensagens do banner e a imagem do background, e o link dos cliques -->
	<?php if (!$isMobile) : ?>
		<div class="slick-banner">
		<div style="--background: linear-gradient(rgba(36, 36, 36, 0.5) 50%, rgba(36, 36, 36, 0.5) 50%), url('<?= $url ?>imagens/banner/banner-1.webp')">
				<div class="content-banner">
					<h1>AF CALDEIRARIA</h1>
					<p>Caldeiraria • Tanques</p>
					<a class="btn" href="<?= $url ?>catalogo" title="Produtos Páginas">Produtos</a>
				</div>
			</div>
			<div style="--background: linear-gradient(rgba(36, 36, 36, 0.7) 50%, rgba(36, 36, 36, 0.7) 50%), url('<?= $url ?>imagens/banner/banner-2.webp')">
				<div>
					<div class="content-banner justify-content-evenly row">
						<div>
							<h2>Qualidade em cada solda, confiança em cada projeto</h2>
							<a class="btn" href="<?= $url ?>sobre-nos" title="Catalogo de Produtos">Mais informações</a>
						</div>
						
					</div>
				</div>
			</div>
			
	
		</div>
	<?php endif; ?>