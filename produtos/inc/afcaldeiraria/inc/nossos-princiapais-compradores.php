<section class="wrapper principais-compradores">
    <div class="principais-compradores-infos">
        <h2>Nossos principais <span>compradores</span></h2>
        <p>A qualidade e eficácia da <?php echo $cliente_minisite ?> atraem compradores de renome. Aqui estão alguns exemplos:</p>
        <a href="<?php $link_mini_site ?>catalogo" class="principais-compradores-button">Conheça nossos produtos</a>
    </div>
    <div class="principais-compradores-img">
        <a href="<?php echo $prefix_includes ?>imagens/principais-compradores/comprador-1.webp" class="lightbox">
            <img src="<?php echo $prefix_includes ?>imagens/principais-compradores/comprador-1.webp" alt="Comprador 1">
        </a>
        <a href="<?php echo $prefix_includes ?>imagens/principais-compradores/comprador-2.webp" class="lightbox">
            <img src="<?php echo $prefix_includes ?>imagens/principais-compradores/comprador-2.webp" alt="Comprador 2">
        </a>
        <a href="<?php echo $prefix_includes ?>imagens/principais-compradores/comprador-3.webp" class="lightbox">
            <img src="<?php echo $prefix_includes ?>imagens/principais-compradores/comprador-3.webp" alt="Comprador 3">
        </a>
        <a href="<?php echo $prefix_includes ?>imagens/principais-compradores/comprador-4.webp" class="lightbox">
            <img src="<?php echo $prefix_includes ?>imagens/principais-compradores/comprador-4.webp" alt="Comprador 4">
        </a>
        <a href="<?php echo $prefix_includes ?>imagens/principais-compradores/comprador-5.webp" class="lightbox">
            <img src="<?php echo $prefix_includes ?>imagens/principais-compradores/comprador-5.webp" alt="Comprador 5">
        </a>
        <a href="<?php echo $prefix_includes ?>imagens/principais-compradores/comprador-6.webp" class="lightbox">
            <img src="<?php echo $prefix_includes ?>imagens/principais-compradores/comprador-6.webp" alt="Comprador 6">
        </a>
         </div>
</section>