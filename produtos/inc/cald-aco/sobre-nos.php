<?
$h1         = 'Sobre nós';
$title      = 'Sobre nós';
$desc       = 'sobre nos';
$key        = 'uuuuuuuuuu, jjjjjjjjjjjj, lllllllllll';
$var        = 'Sobre nós';

include('inc/head.php');
?>

<style>
    .main-sobre-nos {
        justify-content: start;

        & h2 {
            font-size: 2.3rem;
        }

        & p {
            font-size: 1.2rem;
        }
    }

    <?
    include('css/header-script.css');
    include "$linkminisite" . "css/style.css";
    include "$linkminisite" . "css/mpi.css";
    include "$linkminisite" . "css/normalize.css";
    include "$linkminisite" . "css/aside.css";
    ?>
</style>
</head>

<body>

    <header id="nav-menu" aria-label="navigation bar">
        <? include "inc/header-dinamic.php" ?>
    </header>
    <?= $caminho ?>

    <main class="wrapper main-sobre-nos">
    <h2>CALD AÇO LTDA.</h2>
    <p>Demos início a nossas atividades em 4 de outubro de 2013, em nossa sede própria, localizada na Av. Juscelino Kubistchek, 7778, Bairro Benfica da cidade mineira de Juiz de Fora. Inicialmente, a CALD AÇO era composta por três sócios e apenas dois funcionários: um supervisor de tubulação e um mecânico de manutenção. Fazia serviços simples em pequenas empresas, mas sempre executados com total competência e transparência desde o primeiro contato.</p>
    <p>Aos poucos, a empresa foi ganhando espaço no mercado. Hoje, com apenas seis anos de funcionamento, prestamos serviços para grandes empresas, como ArcelorMittal, Paraibuna Embalagens, BD, Mercedes, MRR, UTE/JF em parceria com a Petrobrás, Keitek e Hebert Engenharia, entre outras. Nosso crescimento é um testemunho da <strong>ótima infraestrutura</strong> e do compromisso com a <strong>qualidade</strong>.</p>
    <p>Nossa equipe é formada por <strong>profissionais qualificados</strong>, treinados para oferecer soluções eficazes e seguras a todos os nossos clientes. O investimento contínuo em capacitação e tecnologia nos permite manter um padrão elevado de serviço.</p>
    <p>Oferecemos <strong>serviços especializados</strong> em movimentação e manutenção industrial, adaptando nossas soluções às necessidades específicas de cada cliente. Isso nos permite superar as expectativas e consolidar nossa posição como um fornecedor confiável e essencial no mercado industrial.</p>
    <p>Orgulhosamente brasileira, a CALD AÇO se compromete a manter o mais alto padrão de qualidade e segurança, elevando todas as expectativas e redefinindo os padrões do nosso setor.</p>
</main>


    <? include('inc/footer.php'); ?>
</body>




</html>