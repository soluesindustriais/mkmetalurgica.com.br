<? $h1 = "Informações";
$title  = "Informações";
$desc = "Orce $h1, conheça os    melhores fornecedores, compare hoje com aproximadamente 200 fabricantes ao mesmo tempo";
$key  = "";
include("inc/head.php");
include("inc/informacoes/informacoes-vetPalavras.php"); ?>

<style>
  body {
    scroll-behavior: smooth;
  }

  <?
  include('css/header-script.css');
  include("$linkminisite" . "css/style.css");
  ?>
</style>
</head>

<body> <? include("inc/header-dinamic.php"); ?><main role="main">
    <div class="content">
      <section> <?= $caminho ?> <div class="wrapper-produtos"> <br class="clear">
          <h1 style="text-align: center;  "><?= $h1 ?></h1>
          <article class="full">
            <div class="article-content">
              <p>Nossa Fundação começou suas atividades em 4 de outubro de 2013, com a inauguração de nossa sede na Av. Juscelino Kubistchek, 7778, no Bairro Benfica, em Juiz de Fora, Minas Gerais. Inicialmente, a CALD AÇO LTDA. era formada por três sócios e dois funcionários — um supervisor de tubulação e um mecânico de manutenção — realizando serviços simples para pequenas empresas, porém sempre com um alto padrão de competência e transparência desde o primeiro contato. Ao longo do tempo, nossa empresa expandiu sua presença no mercado. Hoje, com seis anos de operações, atendemos grandes clientes como ArcelorMittal, Paraibuna Embalagens, BD, Mercedes, MRR, UTE/JF em parceria com a Petrobrás, Keitek e Hebert Engenharia.</p>
            </div>
            <ul class="thumbnails-main"> <?php include_once("inc/informacoes/informacoes-categoria.php"); ?> </ul>
          </article>
      </section>
    </div>
  </main>
  </div>
  <!-- .wrapper --> <? include("inc/footer.php"); ?> </body>

</html>