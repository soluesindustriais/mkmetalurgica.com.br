<?php
$clienteAtivo = "ativo";

//HOMEsolucs
// $tituloCliente recebe ($clienteAtivo igual 'inativo') recebe 'Soluções industriais' se não 'Base Mini Site';
$tituloCliente = ($clienteAtivo == 'inativo') ? 'Soluções industriais' : 'Cald Aco';
$tituloCliente = str_replace(' ', '-', $tituloCliente);

$subTituloCliente = 'Comprometida com a segurança';
$bannerIndex = 'imagem-banner-fixo';
$minisite = "inc/" . $tituloCliente . "/";
$subdominio = str_replace(' ', '', $tituloCliente);


//informacoes-vetPalavras
$CategoriaNameInformacoes = "informacoes";

// Conteudo das páginas
$conteudoPagina = [

    "A busca por <strong>serviço de calandragem</strong> de qualidade é fundamental para empresas que necessitam de soluções precisas na modelagem de materiais metálicos. A calandragem é um processo que envolve a deformação plástica de metais e outros materiais através de cilindros, criando peças com curvaturas específicas que são essenciais em diversas aplicações industriais.<br><br>
    Na nossa empresa, o <strong>serviço de calandragem</strong> é realizado com tecnologia de ponta e profissionais altamente qualificados. Garantimos não só a precisão na execução dos projetos, como também a agilidade e a eficiência necessárias para atender demandas de alta complexidade. Seja para a produção de componentes para a construção civil, peças automotivas ou equipamentos industriais, nosso serviço se destaca pela qualidade incomparável.<br><br>
    
    Optar pelo nosso <strong>serviço de calandragem</strong> significa escolher um parceiro que entende as necessidades do seu negócio e está preparado para contribuir na otimização de seus processos e na redução de custos. Com um controle rigoroso de qualidade e uma atenção dedicada a cada detalhe, asseguramos que cada peça produzida atenda às especificações técnicas mais exigentes.<br><br>
    
    Não deixe de consultar um especialista em <strong>serviço de calandragem</strong> da nossa equipe. Estamos prontos para ajudar a elevar a qualidade de seus produtos finais, garantindo assim o sucesso do seu negócio no mercado competitivo de hoje.",

    "Quando se trata de encontrar uma <strong>empresa de calandragem</strong> confiável e eficiente, a qualidade e a precisão não são apenas desejáveis, são essenciais. Nossa empresa se destaca no mercado por fornecer soluções de calandragem que atendem rigorosamente aos padrões de qualidade e às especificações técnicas dos nossos clientes.<br><br>
    Como uma <strong>empresa de calandragem</strong> líder, investimos continuamente em tecnologia e treinamento de nossa equipe para garantir que cada projeto seja executado com a máxima precisão. Nosso processo abrange uma vasta gama de capacidades de calandragem, adaptando-se a diversos materiais e requisitos de conformação, tornando-nos o parceiro ideal para projetos complexos e de grande escala.<br><br>
    
    Além da excelência técnica, nossa <strong>empresa de calandragem</strong> é reconhecida pelo compromisso com os prazos de entrega e pela flexibilidade em atender demandas personalizadas. Entendemos que cada cliente tem necessidades únicas, e por isso, oferecemos serviços personalizados que vão desde a fase de planejamento até a execução final do produto.<br><br>
    
    Se você está buscando uma <strong>empresa de calandragem</strong> que não apenas atenda às suas expectativas, mas as supere em cada detalhe, entre em contato conosco. Estamos prontos para ajudar a transformar seus projetos em realidade com eficiência, qualidade e a precisão que você precisa.",

    "Procurando por uma solução eficiente em <strong>calandragem de chapa</strong>? Nossa empresa se destaca no mercado por fornecer serviços de calandragem de alta precisão, adequados para uma variedade de indústrias e aplicações. Utilizamos tecnologia de ponta e técnicas avançadas para assegurar que cada chapa metálica seja processada conforme as especificações exatas dos nossos clientes.<br><br>
    A <strong>calandragem de chapa</strong> é um processo técnico que exige habilidade e equipamentos especializados. Nossos profissionais são altamente qualificados e possuem vasta experiência no manuseio de diferentes materiais, garantindo um acabamento perfeito e funcionalidade superior. Seja para a indústria de construção, automotiva ou equipamentos industriais, nossa expertise em <strong>calandragem de chapa</strong> é a solução ideal.<br><br>
    
    Oferecemos um serviço personalizado e adaptável, entendendo que cada projeto pode ter requisitos únicos. Nossa equipe está preparada para discutir suas necessidades específicas e oferecer consultoria especializada, assegurando que o resultado final seja exatamente o que você precisa. Com um compromisso com a qualidade e a satisfação do cliente, nossa <strong>calandragem de chapa</strong> é o serviço que você precisa para garantir a eficácia e durabilidade de seus produtos.<br><br>
    
    Para mais informações sobre nossa <strong>calandragem de chapa</strong> ou para discutir um projeto específico, não hesite em entrar em contato conosco. Estamos aqui para fornecer soluções que impulsionam o sucesso dos seus negócios.",

    "A qualidade e precisão nos <strong>serviços de calandragem de chapas</strong> são essenciais para atender às demandas específicas de diversas indústrias. Nossa empresa é especializada em fornecer soluções de calandragem que combinam tecnologia de ponta com a expertise de profissionais altamente qualificados, garantindo resultados que excedem as expectativas dos nossos clientes.<br><br>
    Nossos <strong>serviços de calandragem de chapas</strong> são ideais para projetos que requerem a moldagem de metais em formas precisas, como cilindros, cones e outros contornos complexos. Atendemos a uma ampla gama de setores, incluindo construção civil, automotivo e manufatura industrial, onde a precisão não é apenas um requisito, mas uma necessidade.<br><br>
    
    Comprometidos com a inovação e qualidade, nossos <strong>serviços de calandragem de chapas</strong> são realizados sob rigorosos padrões de controle, assegurando que cada peça seja produzida com a máxima precisão. Além disso, entendemos a importância de prazos na indústria e nos esforçamos para entregar todos os projetos dentro do tempo estipulado, sem comprometer a qualidade ou a especificação técnica dos produtos.<br><br>
    
    Se você busca uma parceria confiável e serviços que contribuam para o sucesso de seus projetos, escolha nossos <strong>serviços de calandragem de chapas</strong>. Entre em contato hoje mesmo para discutir como podemos ajudar no desenvolvimento e realização de suas necessidades específicas em calandragem de chapas.",

    "Na busca por <strong>serviços de solda em geral</strong> que combinem qualidade, precisão e confiabilidade? Nossa empresa é líder no fornecimento de soluções de soldagem que atendem a uma ampla gama de necessidades industriais e comerciais. Com um compromisso inabalável com a excelência, utilizamos as mais avançadas técnicas e equipamentos para garantir que cada projeto seja executado com a máxima eficácia.<br><br>
    Os <strong>serviços de solda em geral</strong> que oferecemos são ideais para construções metálicas, reparos industriais, fabricação de peças e muito mais. Nossos soldadores certificados possuem a experiência e o conhecimento necessários para realizar soldas de alta qualidade em diversos materiais, incluindo aço, alumínio e aço inoxidável, garantindo resultados duradouros e seguros.<br><br>
    
    Entendemos que cada cliente possui necessidades únicas, por isso, nossos <strong>serviços de solda em geral</strong> são totalmente personalizáveis. Trabalhamos de perto com você para entender suas especificações e oferecer soluções que não apenas atendam, mas superem suas expectativas. Além disso, estamos comprometidos com práticas de soldagem seguras e eficientes, assegurando que todos os aspectos do processo sejam realizados de acordo com os mais altos padrões de segurança.<br><br>
    
    Se você precisa de um parceiro confiável que ofereça <strong>serviços de solda em geral</strong> com um histórico comprovado de sucesso, não hesite em entrar em contato conosco. Estamos aqui para fornecer a expertise e o suporte necessários para levar seus projetos ao próximo nível.",

    "Se você está em busca de uma <strong>empresa de soldagem</strong> que ofereça precisão, confiabilidade e resultados superiores, sua pesquisa termina aqui. Somos uma empresa líder em soldagem, reconhecida pela qualidade excepcional de nossos serviços e pela capacidade de atender a uma ampla gama de necessidades industriais e comerciais.<br><br>
    Como <strong>empresa de soldagem</strong>, utilizamos tecnologia avançada e metodologias inovadoras para assegurar que cada projeto seja realizado com a máxima eficácia. Nossa equipe é composta por profissionais altamente qualificados e experientes, capazes de executar soldas complexas em diversos materiais, incluindo aço carbono, aço inoxidável e alumínio.<br><br>
    
    O compromisso com a excelência é o que nos distingue como uma <strong>empresa de soldagem</strong> de confiança. Entendemos a importância dos detalhes em cada projeto e garantimos que todos os procedimentos de soldagem sejam realizados conforme as normas de segurança mais rigorosas. Nosso objetivo é fornecer soluções de soldagem que não apenas atendam, mas superem as expectativas de nossos clientes, garantindo durabilidade e desempenho excepcionais.<br><br>
    
    Seja para projetos de construção, manutenção industrial ou fabricação de produtos personalizados, nossa <strong>empresa de soldagem</strong> está pronta para enfrentar desafios e fornecer resultados que impulsionam o sucesso dos nossos clientes. Entre em contato conosco para discutir como podemos ajudar no seu próximo projeto de soldagem.",

    "Quando se trata de escolher entre <strong>empresas de montagem de estruturas metálicas</strong>, é essencial selecionar uma parceira que não apenas compreenda as complexidades do seu projeto, mas que também ofereça a expertise e a tecnologia necessárias para executá-lo com precisão. Nossa empresa está na vanguarda da montagem de estruturas metálicas, proporcionando serviços que são sinônimos de qualidade e confiabilidade.<br><br>
    Como uma das principais <strong>empresas de montagem de estruturas metálicas</strong>, estamos comprometidos com a excelência em cada fase do projeto. Desde a análise inicial e planejamento até a execução final, garantimos que cada detalhe seja gerenciado com a máxima precisão. Utilizamos materiais de alta qualidade e as mais recentes técnicas de construção para garantir que cada estrutura seja robusta, durável e completamente alinhada às especificações do cliente.<br><br>
    
    Nossa equipe de especialistas é altamente qualificada e experiente, capaz de lidar com projetos de todos os tamanhos e complexidades. Como uma das <strong>empresas de montagem de estruturas metálicas</strong>, oferecemos uma abordagem personalizada para cada cliente, assegurando que as soluções propostas sejam as mais adequadas para suas necessidades específicas. Além disso, seguimos rigorosos padrões de segurança e sustentabilidade, assegurando que cada projeto seja não apenas eficiente, mas também ambientalmente responsável.<br><br>
    
    Se você está procurando por <strong>empresas de montagem de estruturas metálicas</strong> que possam oferecer soluções completas e adaptadas, contate-nos hoje. Estamos prontos para ajudar a transformar seu projeto em realidade, superando as expectativas e entregando resultados excepcionais.",

    "Se você está procurando por serviços de <strong>montagem de estruturas metálicas</strong>, é crucial escolher um parceiro com a expertise necessária para garantir a qualidade e a segurança do seu projeto. Nossa empresa é líder no setor, especializada na <strong>montagem de estruturas metálicas</strong> para uma variedade de aplicações industriais e comerciais.<br><br>
    Com anos de experiência, somos reconhecidos por nossa habilidade em executar projetos de <strong>montagem de estruturas metálicas</strong> com precisão e eficiência. Utilizamos apenas materiais de alta qualidade e as técnicas mais avançadas para assegurar que cada estrutura seja robusta, durável e esteja em conformidade com todas as normas regulamentares.<br><br>
    
    Nossos serviços de <strong>montagem de estruturas metálicas</strong> são abrangentes, incluindo desde o planejamento e a fabricação até a montagem final no local. Nossa equipe de profissionais qualificados trabalha de perto com cada cliente para entender suas necessidades específicas, oferecendo soluções personalizadas que atendem e superam as expectativas.<br><br>
    
    Escolher nossa empresa para a <strong>montagem de estruturas metálicas</strong> significa investir em segurança, eficiência e qualidade. Estamos comprometidos em fornecer resultados que não apenas atendam às suas necessidades operacionais, mas que também contribuam para a sustentabilidade e eficácia a longo prazo de suas instalações. Entre em contato hoje mesmo para discutir como podemos ajudar a elevar seu próximo projeto a um novo patamar.",

    "Quando se trata de entender o <strong>preço de montagem de estrutura metálica</strong>, é essencial considerar não apenas o custo, mas também o valor agregado ao seu projeto. Nossa empresa oferece soluções competitivas em <strong>preço de montagem de estrutura metálica</strong> sem comprometer a qualidade ou a segurança das instalações.<br><br>
    Ao avaliar o <strong>preço de montagem de estrutura metálica</strong>, é importante considerar vários fatores que podem influenciar o custo final, incluindo o tipo de material utilizado, a complexidade do projeto, e as especificações técnicas exigidas. Nossa equipe trabalha para oferecer estimativas precisas e transparentes, garantindo que você entenda todos os aspectos que compõem o custo do seu projeto.<br><br>
    
    Oferecemos preços competitivos no mercado de <strong>preço de montagem de estrutura metálica</strong> e nos esforçamos para maximizar o retorno sobre o investimento dos nossos clientes. Com processos otimizados e uma abordagem focada na eficiência, conseguimos reduzir custos desnecessários, permitindo que nossa oferta seja tanto acessível quanto de alta qualidade.<br><br>
    
    Se você deseja mais informações sobre o <strong>preço de montagem de estrutura metálica</strong> para o seu próximo projeto, entre em contato conosco hoje. Nossos especialistas estão prontos para ajudá-lo a planejar e executar seu projeto dentro do orçamento, com a garantia de resultados que atendem e superam as expectativas.",

    "Quando o assunto é <strong>caldeiraria pesada</strong>, a expertise e a capacidade técnica são fundamentais para garantir que projetos de grande escala sejam realizados com sucesso e segurança. Nossa empresa se destaca no setor de <strong>caldeiraria pesada</strong>, oferecendo soluções completas que abrangem desde o design até a montagem final de estruturas e componentes metálicos de alta complexidade.<br><br>
    A <strong>caldeiraria pesada</strong> envolve trabalhos com chapas de metal de grande espessura, que exigem equipamentos de ponta e profissionais altamente qualificados. Estamos equipados com tecnologia avançada e uma equipe de engenheiros e técnicos experientes, capazes de manejar projetos desafiadores e entregar produtos que atendem às normas de qualidade e segurança mais rigorosas.<br><br>
    
    Nossa abordagem em <strong>caldeiraria pesada</strong> é focada na personalização e na inovação. Entendemos que cada projeto tem suas próprias especificações e desafios únicos, por isso, oferecemos serviços que são meticulosamente adaptados para atender às necessidades específicas de cada cliente. Isso não apenas garante a satisfação completa, mas também otimiza a funcionalidade e a eficiência das estruturas produzidas.<br><br>
    
    Se você está procurando por uma empresa de <strong>caldeiraria pesada</strong> que possa fornecer soluções robustas e duradouras para seus projetos industriais, não hesite em entrar em contato conosco. Nossa experiência e dedicação à qualidade são a garantia de que seu projeto será realizado com o mais alto padrão de excelência."

];


// Criação de página
// Criação Mini site


// Criar página de produto mpi
/* $VetPalavrasProdutos = [
    "exemplo-produto",
]; */

//Criar página de Serviço
$VetPalavrasInformacoes = [
    "servico-de-calandragem",
    "empresa-de-calandragem",
    "calandragem-de-chapa",
    "servicos-de-calandragem-de-chapas",
    "servicos-de-solda-em-geral",
    "empresa-de-soldagem",
    "empresas-de-montagem-de-estruturas-metalicas",
    "montagem-de-estruturas-metalicas",
    "preco-de-montagem-de-estrutura-metalica",
    "caldeiraria-pesada"
];



// Numero do formulario de cotação
$formCotar = 125;



// Informações Geral.php

$nomeSite = ($clienteAtivo == 'inativo') ? 'Soluções industriais' : 'Cald Aco';
$slogan = ($clienteAtivo == 'inativo') ? "Inovando sua indústria com eficiência" : 'Comprometida com a segurança';
$rua = ($clienteAtivo == 'inativo') ? "Rua Alexandre Dumas" : 'Av. Juscelino Kubischek, 7778';
$bairro = ($clienteAtivo == 'inativo') ? "Santo Amaro" : 'Benfica, Juiz de Fora.';
$cidade = ($clienteAtivo == 'inativo') ? "São Paulo" : 'Minas Gerais';
$UF = ($clienteAtivo == 'inativo') ? "SP" : 'MG';
$cep = ($clienteAtivo == 'inativo') ? "CEP: 04717-004" : 'CEP: 36092-060';
$imgLogo = ($clienteAtivo == 'inativo') ? 'logo-site.webp' : 'logo-cliente-fixo.svg';
$emailContato = ($clienteAtivo == 'inativo') ? '' : 'comercial@sanseitalhas.com.br';
$instagram =  ($clienteAtivo == 'inativo') ? 'https://www.instagram.com/solucoesindustriaisoficial?igsh=MWtvOGF4aXBqODF3MQ==' : 'https://www.instagram.com/sanseitalhasoficial/';
$facebook =  ($clienteAtivo == 'inativo') ? 'https://www.facebook.com/solucoesindustriais/' : 'https://www.facebook.com/sanseitalhasoficial/';



?>


<!-- VARIAVEIS DE CORES -->

<style>
    :root {
        --azul-solucs: #2d2d2f;
        --second-color: #dd3333;
        --terceira-cor: #2d2d2f;
        --font-primary: "Poppins";
        --font-secundary: "Poppins";
    }
</style>
