<div class="clear"></div>
<footer>
	<div class="wrapper">
		<div class="contact-footer">
			<address>
				<span><?= $nomeSite . " - " . $slogan ?></span>
				<?= $rua . " - " . $bairro ?> <br>
				<?= $cidade . " - " . $UF . " - " . $cep ?>
			</address>

			<br>
		</div>
		<div class="menu-footer">

			<!-- quando tiver no minisite -->
			<!-- <nav>
				<ul>
					<?php

					foreach ($menu as $key => $value) {
						if ($sigMenuPosition !== false && $key == $sigMenuPosition) include('inc/menu-footer-sig.php');
						echo '
								<li>
									<a rel="nofollow" href="' . (strpos($value[0], 'http') !== false ? $value[0] : $linksubdominio . $value[0]) . '" title="' . ($value[1] == 'Home' ? 'Página inicial' : $value[1]) . '" ' . (strpos($value[0], 'http') !== false ? 'target="_blank"' : "") . '>' . $value[1] . '</a>
								</li>
								';
					}

					?>
					<li><a href="<?= $linksubdominio ?>mapa-site" title="Mapa do site <?= $nomeSite ?>">Mapa do site</a></li>
				</ul>
			</nav> -->


			<nav>
				<ul>
					<?php

					foreach ($menu as $key => $value) {
						if ($sigMenuPosition !== false && $key == $sigMenuPosition) include('inc/menu-footer-sig.php');
						echo '
								<li>
									<a rel="nofollow" href="' . (strpos($value[0], 'http') !== false ? $value[0] : $linksubdominio . $value[0]) . '" title="' . ($value[1] == 'Home' ? 'Página inicial' : $value[1]) . '" ' . (strpos($value[0], 'http') !== false ? 'target="_blank"' : "") . '>' . $value[1] . '</a>
								</li>
								';
					}

					?>
					<li><a href="<?= $linksubdominio ?>mapa-site" title="Mapa do site <?= $nomeSite ?>">Mapa do site</a></li>
				</ul>
			</nav>
		</div>
		<br class="clear">
	</div>
</footer>
<div class="copyright-footer">
	<div class="wrapper">
		Copyright © <?= $nomeSite ?>. (Lei 9610 de 19/02/1998)
		<div class="selos">
			<a rel="nofollow" href="https://validator.w3.org/nu/?showsource=yes&doc=<?= $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" target="_blank" title="HTML5 W3C"><i class="fa fa-html5"></i> <strong>W3C</strong></a>
			<a rel="nofollow" href="https://jigsaw.w3.org/css-validator/validator?uri=<?= $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>&profile=css3svg&usermedium=all&warning=1&vextwarning=&lang=pt-BR" target="_blank" title="CSS W3C"><i class="fa fa-css3"></i> <strong>W3C</strong></a>
		</div>
	</div>
</div>
<?
include "$linkminisite" . "inc/header-scroll.php";
include "$linkminisite" . "inc/header-fade.php";
?>


<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>


<script defer src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
<script defer src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
<script defer src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
<script defer src="https://painel.solucoesindustriais.com.br/js/sdk/formBuilderSdk.js"></script>

<!-- fonts -->
<link href="https://fonts.googleapis.com/css2?family=Catamaran:wght@100..900&family=Dosis:wght@200..800&display=swap" rel="stylesheet">
<!-- <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">  -->
<!-- <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,400;0,600;1,400;1,600&family=Oswald:wght@200;300;700&display=swap" rel="stylesheet"> -->
<!-- fim fonts -->

<!-- ANALYTICS -->
<!-- Código gtag.js global e local -->
<script defer src="https://www.googletagmanager.com/gtag/js?id=G-D23WW3S4NC"></script>
<script defer>
	// Configuração específica do site
	window.dataLayer = window.dataLayer || [];

	function gtag() {
		dataLayer.push(arguments);
	}
	gtag('js', new Date());
	// ID da propriedade específica do site
	gtag('config', '<?= $tagManager ?>');
</script>
<!-- fim ANALYTICS -->
<? include "$linkminisite" . "inc/fancy.php" ?>
<? include "$linkminisite" . "inc/LAB.php" ?>
<script>
	initReadMore('.ReadMore', {
		collapsedHeight: 200,
		heightMargin: 20
	});
</script>