<div class="modal-btn">
  <div class="form-modal">

    <div class="close-modal-div">
      <i class="fa-solid fa-circle-xmark close-btn"></i>
    </div>

    <div class="grid-form">
      <div class="modal-section-img">
        <h2>Dúvidas?</h2>
        <p>Solicite o seu orçamento de graça!</p>
        <img src="<?= $linkminisite ?>imagens/orcamento-amico.png" alt="Orçamento amico">
      </div>
      <div class="formulario-section">
        <div data-sdk-form-builder-solucoes data-form-id="<?= $formCotar ?>" class="form-soluc"></div>
      </div>
    </div>

  </div>
</div>

<style>


  .modal-section-img {
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
  }

  .modal-section-img>h2 {
    font-size: 2em;
  }
  .modal-section-img>p {
    font-size: 1.2em;
  }

  .formulario-section {
    display: flex;
    justify-content: center;
    border-left: 1px solid #c8c8c8;
  }

  .close-modal-div {
    width: 100%;
    text-align: end;
  }

  .modal-section-img>img {
    max-height: 400px;
    max-width: 400px;
  }

  .grid-form {
    display: grid;
    grid-template-columns: 1fr 1fr;
    gap: 20px;
  }

  .form-modal {
    background-color: #fff;
    padding: 20px;
    width: auto;
    border-radius: 4px;
    animation: myAnim 0.2s ease 0s 1 normal forwards;
  }

  @keyframes myAnim {
    0% {
      transform: scale(0.5);
    }

    100% {
      transform: scale(1);
    }
  }


  .text-form-modal {
    width: 100%;
    display: flex;
    justify-content: center;
  }

  .text-form-modal>h2 {
    font-size: 1.4rem;
    font-weight: 500;
    color: #2d2d2f;
  }


  .modal-btn {
    width: 100%;
    height: 100vh;
    /* Adicionar altura se necessário */
    z-index: 800;
    background-color: rgba(0, 0, 0, 0.5);
    /* Cor semi-transparente */
    display: none;
    /* Esconder inicialmente */
    align-items: center;
    justify-content: center;
    position: fixed;
    /* Para posicionar sobre o conteúdo */
    top: 0;
    left: 0;
  }

  .close-btn {
    top: 0;
    /* Alinha ao topo */
    left: 0;
    /* Alinha à esquerda */
    cursor: pointer;
    font-size: 1.8rem;
    margin: 10px;
    /* Adiciona um pouco de espaço em volta do botão */
    color: red;
    transition: 0.6s;
  }

  .close-btn:hover {
    transform: scale(1.1);
  }


  .form-soluc {
    width: 80%;
  }

  .form-soluc>form {
    display: flex;
    flex-direction: column;
    padding: 10px 20px;
    gap: 10px;
  }

  .form-soluc>.btn {
    display: flex !important;
    width: 100%;
    justify-content: center;
    align-items: center;
    height: 50px;
    font-size: 1rem;
    width: 300;
    color: #fff;
    font-size: 1.3em;
    background-color: var(--color-tertiary);
    border: 0px !important;
  }

  .form-group {
    display: flex;
    width: 100%;
    max-width: 100%;
    justify-content: center;
    align-items: flex-start;
    gap: 10px;
    font-family: "Poppins", sans-serif;
  }

  .form-group>label {
    display: flex;
    gap: 8px;
    font-weight: 400;
    color: #2d2d2f;
    font-size: 1em;
  }

  .form-group>input {
    border-radius: 3px;
    padding: 10px;
    width: -webkit-fill-available;
    outline: none;
    border-radius: 4px;
  }


  .form-group>textarea {
    resize: none;
    border: 1px solid #ccc !important;
    height: 80px;
    border-radius: 3px;
    padding: 10px;
    outline: none;
    width: -webkit-fill-available;
    ;
  }

  .btn {
    width: 100%;
    font-size: 1.4em;
    background-color: green;
    color: #fff;
  }

  @media screen and (max-width: 1000px) {
  .grid-form{
    grid-template-columns: 1fr;
  }

  .modal-section-img > img {
    display: none;
  }

  .formulario-section {
    border: 0px;
  }

  .form-modal {
    width: 100%;
    margin: 0px 20px;
  }
}

@media screen and (max-width: 600px) {

  .form-modal {
    width: 100%;
    margin: 0px 30px;
  }

  .form-soluc > form {
    padding: 0;
  }
}

@media screen and (max-width: 500px) {

.grid-form {
  display: flex;
  flex-direction: column;
}

.form-modal {
  width: 100%;
  margin: 0px 10px;
}

.form-soluc > form {
  padding: 0;
}

.formulario-section {
  width: 100%;
}
}

</style>

<script>
  // Capturar os elementos
  const btnCotar = document.querySelector('.btn-cotar');
  const modal = document.querySelector('.modal-btn');
  const closeBtn = document.querySelector('.modal-btn .close-btn');

  // Função para abrir o modal
  btnCotar.addEventListener('click', () => {
    modal.style.display = 'flex';
  });

  // Função para fechar o modal
  closeBtn.addEventListener('click', () => {
    modal.style.display = 'none';
  });

  // Fechar o modal ao clicar fora dele
  window.addEventListener('click', (e) => {
    if (e.target === modal) {
      modal.style.display = 'none';
    }
  });
</script>