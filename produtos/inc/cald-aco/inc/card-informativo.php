<section class="cards-informativo flex-wrap-mobile">
    <div class="card">
        <div class="mini-wrapper">
            <div class="card-ico">
                <i class="fa-solid fa-industry"></i>
            </div>
            <div class="card-content">
                <h2 class="color-secundary-mpi">Industria Eficiente</h2>
                <p class="p-card">Clientes <br>Satisfeitos</p>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="mini-wrapper">
            <div class="card-ico">
                <i class="fa-solid fa-truck"></i>
            </div>
            <div class="card-content">
                <h2 class="color-secundary-mpi">Logistica Rápida</h2>
                <p class="p-card">Entrega em todo <br>Brasil</p>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="mini-wrapper">
            <div class="card-ico">
                <i class="fa-solid fa-tree"></i>
            </div>
            <div class="card-content">
                <h2 class="color-secundary-mpi">Sustentabilidade</h2>
                <p class="p-card">Pensamos no meio <br>ambiente</p>
            </div>
        </div>
    </div>
</section>