<section class="wrapper flex-wrap-mobile section-content">
	<div class="section-text">
		<h4 class="sub-title">O que nós fornecemos</h4>
		<h2 class="text-center title">Nossos Serviços</h2>

		<ul class="list-benefits d-flex flex-wrap-mobile justify-content-between">
			<li>
				<div class="icon-services"><i class="fa-solid fa-building"></i></div>
				<h3 class="sub-title">Ótima Infraestrutura</h3>
				<p>Iniciamos nossas atividades em 2013 em nossa sede própria, localizada na Av. Juscelino Kubistchek, Juiz de Fora. Com instalações modernas e equipamentos de ponta, garantimos a eficiência e a segurança dos serviços prestados. <strong>Visite-nos para conhecer nossa infraestrutura!</strong></p>
			</li>

			<li>
				<div class="icon-services"><i class="fa-solid fa-users"></i></div>
				<h3 class="sub-title">Profissionais Qualificados</h3>
				<p>A CALD AÇO conta com uma equipe de profissionais altamente qualificados, incluindo supervisores de tubulação e mecânicos de manutenção. Nossa equipe é treinada para executar todos os serviços com competência e transparência. <strong>Entre em contato e saiba mais sobre nossa equipe!</strong></p>
			</li>

			<li>
				<div class="icon-services"><i class="fa-solid fa-tools"></i></div>
				<h3 class="sub-title">Serviços Especializados</h3>
				<p>Desde pequenos serviços até grandes projetos para empresas renomadas como ArcelorMittal e Mercedes, oferecemos soluções personalizadas e especializadas. Nosso portfólio inclui parcerias com grandes nomes do setor industrial. <strong>Descubra como podemos ajudar sua empresa!</strong></p>
			</li>
		</ul>

	</div>
</section>