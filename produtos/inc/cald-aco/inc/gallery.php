<ul class="mpi-gallery">
  <? $imagens = glob("$linkminisite"."imagens/informacoes/".$urlPagina."-{,[0-6 ]}[0-6 ].webp", GLOB_BRACE);
  foreach ($imagens as $key => $imagem): ?>
    <li>
      <a href="<?=$url.$imagem?>" data-fancybox="group1" class="lightbox" title="<?=$h1?>" data-caption="<?=$h1?>">
        <img src="<?=$url.$imagem?>" alt="<?=$h1?>" title="<?=$h1?>">
      </a>
    </li>
  <? endforeach; ?>
</ul>
<p class="alerta">Clique nas imagens para ampliar</p>